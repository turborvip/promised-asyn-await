// Xin số điện thoại -> sạc pin -> gọi

const xinSoDt = () => {
    console.log('Gọi điện cho thằng bạn : cho tao xin số thằng Nam');
    console.log('Đang tìm....');

    const thaoTacTimKiem = new Promise((resolve, reject) => {
        let daTimDuoc;

        setTimeout(() => {
            daTimDuoc = true;

            if (daTimDuoc) {
                let sdt = 123456;
                console.log(`Đây rồi. Số của nó là ${sdt}`);
                resolve(sdt);
            } else {
                reject('Tao không có số thằng Nam');
            }
        }, 1000)
    })

    return thaoTacTimKiem;
}

const sacPin = () => {
    console.log('Đang sạc pin....loading');

    let chayPin;
    const thaoTacSacPin = new Promise((resolve, reject) => {
        setTimeout(() => {
            chayPin = true;
            if (!chayPin) {
                console.log('Đã sạc đầy.Bắt đầu gọi cho Nam');
                resolve();
            } else {
                reject('Cháy pin rồi ngu lắm')
            }
        }, 1500)
    })

    return thaoTacSacPin;
}

const goiChoNam = (sdt) => {
    console.log(`Đang nói chuyện với Nam ${sdt}`);
}

//Hành Động
const hanhDong = async () => {
    try {
        const sdt = await xinSoDt();
        await sacPin();
        goiChoNam(sdt);

    } catch (error) {
        console.log(error)
    }
}
hanhDong();