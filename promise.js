// Xin số điện thoại 
// const xinSoDt = () => {
//     console.log('Gọi điện cho thằng bạn : cho tao xin số thằng Nam');
//     console.log('Đang tìm....');

//     const thaoTacTimKiem = new Promise((resolve, reject) => {
//         let daTimDuoc;

//         setTimeout(() => {
//             daTimDuoc = false;

//             if (daTimDuoc) {
//                 let sdt = 123456;
//                 console.log(`Đây rồi. Số của nó là ${sdt}`);
//                 resolve(sdt);
//             } else {
//                 reject('Tao không có số thằng Nam');
//             }
//         }, 1000)
//     })

//     return thaoTacTimKiem;
// }

// xinSoDt().then(sodt => console.log(`Đã xin được sô thằng Nam ${sodt}`))
//     .catch(err => console.log(err))


// Xin số điện thoại -> sạc pin -> gọi

const xinSoDt = () => {
    console.log('Gọi điện cho thằng bạn : cho tao xin số thằng Nam');
    console.log('Đang tìm....');

    const thaoTacTimKiem = new Promise((resolve, reject) => {
        let daTimDuoc;

        setTimeout(() => {
            daTimDuoc = true;

            if (daTimDuoc) {
                let sdt = 123456;
                console.log(`Đây rồi. Số của nó là ${sdt}`);
                resolve(sdt);
            } else {
                reject('Tao không có số thằng Nam');
            }
        }, 1000)
    })

    return thaoTacTimKiem;
}

const sacPin = (std) => {
    console.log('Đang sạc pin....loading');

    let chayPin;
    const thaoTacSacPin = new Promise((resolve, reject) => {
        setTimeout(() => {
            chayPin = false;
            if (!chayPin) {
                console.log('Đã sạc đầy.Bắt đầu gọi cho Nam');
                resolve(std);
            } else {
                reject('Cháy pin rồi ngu lắm')
            }
        }, 1500)
    })

    return thaoTacSacPin;
}

const goiChoNam = (sdt) => {
    console.log(`Đang nói chuyện với Nam ${sdt}`);
}


xinSoDt().then(
    (sodt) => {
        sacPin(sodt)
            .then(sdt => goiChoNam(sdt))
            .catch(err => console.log(err))
    }
)
    .catch(err => console.log(err))
