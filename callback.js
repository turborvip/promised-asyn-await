//Van de
const xinSoDt = () => {
    let sodt;

    console.log('Goi dien cho thằng bạn : Cho tao xin số thằng Nam')

    console.log('Thằng bạn tìm số thằng nam')

    setTimeout(
        () => {
            sodt = 123456;
            console.log('Đây rồi thằng bạn tìm được số rồi')
        }, 1000
    ) // Hứa trước trong tương lai 

    return sodt //nó trả về trước promise(lời hứa) trả về undefined
}

// const soDtDaXin = xinSoDt();

// console.log(soDtDaXin);


//Cách giải quyết sai 
const xinSoDt2 = () => {
    let sodt;

    console.log('Goi dien cho thằng bạn : Cho tao xin số thằng Nam')

    console.log('Thằng bạn tìm số thằng nam')

    setTimeout(
        () => {
            sodt = 123456;
            console.log('Đây rồi thằng bạn tìm được số rồi')
        }, 1000
    ) // Hứa trước trong tương lai 

    console.log(sodt)
}

// const soDtDaXin = xinSoDt2();

// console.log(xinSoDt2);

//Cách giải quyết xấu 
const xinSoDt3 = () => {
    let sodt;

    console.log('Goi dien cho thằng bạn : Cho tao xin số thằng Nam')

    console.log('Thằng bạn tìm số thằng nam')

    setTimeout(
        () => {
            sodt = 123456;
            console.log('Đây rồi thằng bạn tìm được số rồi')
        }, 1000
    ) // Hứa trước trong tương lai 
    setTimeout(
        () => {
            console.log(sodt)
        }, 1500
    ) // Hứa trước trong tương lai 
    console.log(sodt)
}

// const soDtDaXin = xinSoDt3();

// console.log(xinSoDt3);


//Cách sửa đúng sử dụng hàm gọi lại (Callback function)
// const xinSoDt4 = (callback) => {
//     let sodt;

//     console.log('Goi dien cho thằng bạn : Cho tao xin số thằng Nam')

//     console.log('Thằng bạn tìm số thằng nam')

//     setTimeout(
//         () => {
//             sodt = 123456;
//             console.log('Đây rồi thằng bạn tìm được số rồi')
//             callback(sodt)
//         }, 1000
//     ) // Hứa trước trong tương lai 
// }

// const sauKhiNhanDuocSDT = (sodtDaNhan) => console.log(`Đây là số điện thoại thằng Nam:${sodtDaNhan}`)

// xinSoDt4(sauKhiNhanDuocSDT)

// Vấn đề mới : quá nhiều hàm gọi lại (callback functions)
const xinSoDt5 = (hamGoiLaiSauKhiTimRa) => {
    let sodt;

    console.log('Goi dien cho thằng bạn : Cho tao xin số thằng Nam')

    console.log('Thằng bạn tìm số thằng nam')

    setTimeout(
        () => {
            sodt = 123456;
            console.log('Đây rồi thằng bạn tìm được số rồi')
            hamGoiLaiSauKhiTimRa(sodt, goiChoNam)
        }, 1000
    )
}

const sacPin = (soDT, hamGoiLaiSauKhiSacPin) => {
    console.log('Doi tí sạc pin đã');
    setTimeout(() => {
        console.log('Sạc Pin xong . Bat đầu gọi cho Nam');
        hamGoiLaiSauKhiSacPin(soDT);
    }, 2000)
}

goiChoNam = (sodt) => {
    console.log(`Đang nói chuyện với Nam: ${sodt}`)
}

// tiến hành 
xinSoDt5(sacPin)
// Giải thích 2 call back trồng lên nhau
// => vấn đề là rất khó viết và cực kì phức tạp